#include "stm32f429xx.h"
#include "stm32f4xx_ll_adc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_tim.h"

int main()
{
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);
  
  LL_TIM_SetCounterMode(TIM3, LL_TIM_COUNTERMODE_UP);
  LL_TIM_SetPrescaler(TIM3, 200 - 1);
  LL_TIM_SetAutoReload(TIM3, 8000 - 1);
  LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_UPDATE);
  LL_TIM_EnableCounter(TIM3);
  
  LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_EXT_TIM3_TRGO);
  LL_ADC_REG_SetSequencerLength(ADC1, LL_ADC_REG_SEQ_SCAN_DISABLE);
  LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_TEMPSENSOR);
  LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_SINGLE);
  LL_ADC_SetCommonPathInternalCh(ADC, LL_ADC_PATH_INTERNAL_TEMPSENSOR);
  LL_ADC_EnableIT_EOCS(ADC1);
  LL_ADC_Enable(ADC1);
  LL_ADC_REG_StartConversionExtTrig(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
  
  NVIC_EnableIRQ(ADC_IRQn);
  
  while(1);
}

void ADC_IRQHandler(void)
{
  double V_sense = LL_ADC_REG_ReadConversionData12(ADC1)/4096.*3.3;
  double temp = (V_sense - 0.76)/2.5e-3+25;
  printf("%f\n",temp);
}