#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_tim.h"

int main()
{
  //Clocking for GPIOG and TIM2
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
  
  //Set up PG13 & PG14 to output
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_14, LL_GPIO_MODE_OUTPUT);
  //Set PG14 for alternative blinking
  LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_14);
  
  //Configure TIM2
  LL_TIM_SetPrescaler(TIM2, 1000);
  LL_TIM_SetCounterMode(TIM2, LL_TIM_COUNTERMODE_UP);
  LL_TIM_SetAutoReload(TIM2, 8000);
  LL_TIM_EnableIT_UPDATE(TIM2);
  LL_TIM_EnableCounter(TIM2);
  
  //Configure NVIC
  NVIC_EnableIRQ(TIM2_IRQn);
  
  //Infinite loop
  while(1);
}

void TIM2_IRQHandler(void)
{
  LL_TIM_ClearFlag_UPDATE(TIM2);
  LL_GPIO_TogglePin(GPIOG, LL_GPIO_PIN_13 | LL_GPIO_PIN_14);
}