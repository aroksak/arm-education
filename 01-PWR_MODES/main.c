#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_exti.h"

#define USE_STOP_MODE 1

int main()
{
  //Clocking for GPIOA&G and TIM2
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG | LL_AHB1_GRP1_PERIPH_GPIOG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
  
  //Set up PG13 & PG14 to output
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_14, LL_GPIO_MODE_OUTPUT);
  
  //Configure EXTI to enable interrupts on PA0
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_0);
  
  //Configure TIM2
  LL_TIM_SetPrescaler(TIM2, 1000);
  LL_TIM_SetCounterMode(TIM2, LL_TIM_COUNTERMODE_UP);
  LL_TIM_SetAutoReload(TIM2, 8000);
  LL_TIM_EnableIT_UPDATE(TIM2);
  LL_TIM_EnableCounter(TIM2);
  
  if(USE_STOP_MODE)
  {
    //STOP mode will be entered on WFI command
    LL_LPM_EnableDeepSleep();
    LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_MAINREGU);
  } else {
    //SLEEP mode will be enterred on WFI command
    LL_LPM_EnableSleep();
  }
  
  //Configure NVIC
  NVIC_EnableIRQ(TIM2_IRQn);
  NVIC_EnableIRQ(EXTI0_IRQn);
  
  //Infinite loop
  while(1)
  {
    __WFI(); //Enter SLEEP/STOP mode with WAKE_FROM_INTERRUPT
  }
}

void TIM2_IRQHandler(void)
{
  LL_TIM_ClearFlag_UPDATE(TIM2);
  LL_GPIO_TogglePin(GPIOG, LL_GPIO_PIN_13);
}

void EXTI0_IRQHandler(void)
{
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
  LL_GPIO_TogglePin(GPIOG, LL_GPIO_PIN_14);
}